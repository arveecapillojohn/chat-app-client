import './App.css';
import React, {useState} from 'react';
import io from 'socket.io-client';


import { BrowserRouter as Router, Route } from 'react-router-dom';
import Join from './components/Join/Join';
import Chat from './components/Chat/Chat';


function App(){
    return (
        <Router>
            <Route path='/' exact component={Join} />
            <Route path='/chat' component={Chat} />
        </Router>
    )  
}

export default App;







  



    // const [message, setMessage] = useState('')


    // const onSend = (e) => {
    //     setMessage(e.target.value)
    // }
    // return (
    //     <div className='App'>
    //         <header className='app-header'>
    //             <h1>The Chat Box</h1>
    //             <form>
    //                 <input 
    //                     type='text'
    //                     value='message'
    //                     onChange={onSend}
    //                 />
    //             </form>
    //         </header>
    //     </div>    
    // )