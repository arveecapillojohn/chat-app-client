import React from 'react';
import './Input.css';


const Input = ({message, sendMessage, setMessage}) =>{

    return (
        <form className='input-form'>
            <input 
                className='input'
                type='text'
                placeholder='Send a message...'
                value={message}
                onChange={(e) => setMessage(e.target.value)}
                onKeyPress={event => event.key === 'Enter' ? sendMessage(event) : null}
            />
            <button className='input-button' onClick={e => sendMessage(e)}>Send</button>
        </form>
    )
}

export default Input;


