import React from 'react';
import './Message.css';

const Message = ({message: {user, text}, name}) => {
    let sentByThisUser = false;
    const userName = name.trim().toLowerCase();

    if(user === userName){
        sentByThisUser = true;
    }

    return (
        sentByThisUser
        ? (
            <div className='message-container justifyEnd'>
                <p className='message-sent'>{userName}</p>
                <div className='message-box'>
                    <p className='message-text'>{text}</p>
                </div>
            </div>
        ) : (
            <div className='message-container justifyStart'>
                <div className='message-box'>
                    <p className='message-text'>{text}</p>
                </div>
                <p className='message-sent'>{user}</p>
            </div>
        )
    )
}

export default Message
