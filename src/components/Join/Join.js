import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import './Join.css'

export const Join = () => {
    const [name, setName] = useState(''); 
    const [room, setRoom] = useState(''); 


    const preventClick = (e) => {
        if(!name || !room){
            e.preventDefault()
        } else {
            return null
        }
    } 
    return (
        <div className='join-outer-container'>
            <div className='join-inner-container'>
                <h1 className='heading'>Join</h1>
            </div>
            <div>
                <input 
                    placeholder='Name'
                    className='join-input'
                    type='text'
                    onChange={(e) => setName(e.target.value)}
                />
            </div>
            <div>
                <input 
                    placeholder='Room'
                    className='join-input mt-20'
                    type='text'
                    onChange={(e) => setRoom(e.target.value)}
                />
            </div>
            <Link onClick={preventClick} to={`/chat?name=${name}&room=${room}`}>
                <button className='button mt-20' type='submit'>Sign in</button>
            </Link>
        </div>
    )
}

export default Join;